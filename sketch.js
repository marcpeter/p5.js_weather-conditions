let weather_conditions = '04n';
let diameter = 150;
let switching = 0;
let switcher_stars = 0
let x_position_clouds = 1050
let x_position_clouds2 = 650
let x_position_clouds3 = 900
let x_position_clouds4 = 1100
let x_position_clouds5 = 550
let x_position_clouds6 = 700
let x_position_clouds7 = 600
let pos_star_y = 0
let pos_star_x = 0
let star_pos_list = []
    for(i = 0; i < 40; i++){
      let inner_list = [Math.random(), Math.random()]
      star_pos_list.push(inner_list)
    }
let counter_stars = 0 



function setup() {
  createCanvas(600, 200);
  noStroke()
  frameRate(30)
}

if(weather_conditions === '01d'){
  function draw() {
    if(switching === 0){
      diameter -= 0.2
      if(diameter <= 120){
        switching = 1
      }
    } 
    else if(switching === 1){
      diameter += 0.2
      if(diameter >= 150){
        switching = 0
      }
    } 
    

    background(0,0,0)
    
    ellipse(width/2,height/2, diameter, diameter)
    fill(255,204,0)

  }
}

if(weather_conditions === '02d'){
  function draw(){

    background(0,0,0)
    fill(255,204,0)
    ellipse(width/2,height/2, diameter, diameter)
    x_position_clouds -= 1
    if(x_position_clouds === -100){
      x_position_clouds = 650
    }
    x_position_clouds2 -= 1
    if(x_position_clouds2 === -100){
      x_position_clouds2 = 650
    }
    x_position_clouds3 -= 1
    if(x_position_clouds3 === -100){
      x_position_clouds3 = 650
    }
    

    function cloudOne(position,position_y, width_cloud){
      fill(210, 210, 210)
      ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
      ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
      ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
      ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
    }
    cloudOne(x_position_clouds,160, 180)
    cloudOne(x_position_clouds2 ,140, 140)
    cloudOne(x_position_clouds3 ,90, 120)
    
  }
}
if(weather_conditions === '01n'){

  function draw(){
    background(0,0,0);
    counter_stars += 1
    
    if(counter_stars % 50 === 0){
      switcher_stars = 0
    }
    if(counter_stars % 100 === 0){
      switcher_stars = 1
    }
    if(counter_stars % 150 === 0){
      switcher_stars = 2
    }

    if(switcher_stars === 1){
      for(i = 21; i< 25; i++){
        push();
        translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
        star(0, 0, 3, 7, 5); 
        pop();
      }
    }
    if(switcher_stars === 2){
      for(i = 26; i< 30; i++){
        push();
        translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
        star(0, 0, 3, 7, 5); 
        pop();
      }
    }
   

    
    for(i = 0; i<20; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 2, 5, 5); 
          pop();
        }
      
     
    fill(255,204,0)
    ellipse(width/2,height/2, diameter, diameter)
    fill(0)
    ellipse(width/2+40,height/2, diameter-30, diameter-15)
 
  }
  function star(x, y, radius1, radius2, npoints) {
    var angle = TWO_PI / npoints;
    var halfAngle = angle/2.0;
    fill(255,255,255)
    beginShape();
    for (var a = - 0.5 * PI / npoints; a < TWO_PI; a += angle) {
      var sx = x + cos(a) * radius2;
      var sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a+halfAngle) * radius1;
      sy = y + sin(a+halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
  
  
    
}
if(weather_conditions === '02n'){

  function draw(){
    background(0,0,0);
    counter_stars += 1
    
    if(counter_stars % 50 === 0){
      switcher_stars = 0
    }
    if(counter_stars % 100 === 0){
      switcher_stars = 1
    }
    if(counter_stars % 150 === 0){
      switcher_stars = 2
    }

    if(switcher_stars === 1){
      for(i = 21; i< 25; i++){
        push();
        translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
        star(0, 0, 3, 7, 5); 
        pop();
      }
    }
    if(switcher_stars === 2){
      for(i = 26; i< 30; i++){
        push();
        translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
        star(0, 0, 3, 7, 5); 
        pop();
      }
    }
   

    
    for(i = 0; i<20; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 2, 5, 5); 
          pop();
        }
      
     
    fill(255,204,0)
    ellipse(width/2,height/2, diameter, diameter)
    fill(0)
    ellipse(width/2+40,height/2, diameter-30, diameter-15)

    x_position_clouds -= 1
    if(x_position_clouds === -100){
      x_position_clouds = 650
    }
    x_position_clouds2 -= 1
    if(x_position_clouds2 === -100){
      x_position_clouds2 = 650
    }
    x_position_clouds3 -= 1
    if(x_position_clouds3 === -100){
      x_position_clouds3 = 650
    }
    

    function cloudOne(position,position_y, width_cloud){
      fill(210, 210, 210)
      ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
      ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
      ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
      ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
    }
   
    cloudOne(x_position_clouds2 ,140, 140)
    cloudOne(x_position_clouds3 ,90, 120)
    
  }
 
  
  function star(x, y, radius1, radius2, npoints) {
    var angle = TWO_PI / npoints;
    var halfAngle = angle/2.0;
    fill(255,255,255)
    beginShape();
    for (var a = - 0.5 * PI / npoints; a < TWO_PI; a += angle) {
      var sx = x + cos(a) * radius2;
      var sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a+halfAngle) * radius1;
      sy = y + sin(a+halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
}
if(weather_conditions === '03d'){
    function draw(){
  
      background(0,0,0)
      fill(255,204,0)
      ellipse(width/2,height/2, diameter, diameter)
      x_position_clouds -= 1.2
      if(x_position_clouds <= -100){
        x_position_clouds = 650
      }
      x_position_clouds2 -= 1
      if(x_position_clouds2 === -100){
        x_position_clouds2 = 650
      }
      x_position_clouds3 -= 2
      if(x_position_clouds3 === -100){
        x_position_clouds3 = 650
      }
      x_position_clouds4 -= 1.5
      if(x_position_clouds3 <= -100){
        x_position_clouds3 = 650
      }
      
  
      function cloudOne(position,position_y, width_cloud){
        fill(210, 210, 210)
        ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
        ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
      }
      cloudOne(x_position_clouds,160, 180)
      cloudOne(x_position_clouds2 ,140, 140)
      cloudOne(x_position_clouds3 ,90, 120)
      cloudOne(x_position_clouds4 ,60, 120)
      
    }
  }

  if(weather_conditions === '04d'){
    function draw(){
  
      background(0,0,0)
      fill(255,204,0)
      ellipse(width/2,height/2, diameter, diameter)
      x_position_clouds -= 1.7
      if(x_position_clouds <= -100){
        x_position_clouds = 650
      }
      x_position_clouds2 -= 1
      if(x_position_clouds2 === -100){
        x_position_clouds2 = 650
      }
      x_position_clouds3 -= 2
      if(x_position_clouds3 === -100){
        x_position_clouds3 = 650
      }
      x_position_clouds4 -= 0.7
      if(x_position_clouds4 <= -100){
        x_position_clouds4 = 650
      }
      x_position_clouds5 -= 0.9
      if(x_position_clouds5 <= -100){
        x_position_clouds5 = 650
      }
      x_position_clouds6 -= 1.2
      if(x_position_clouds6 <= -100){
        x_position_clouds6 = 650
      }
      x_position_clouds7 -= 1.4
      if(x_position_clouds7 <= -100){
        x_position_clouds7 = 650
      }
      
  
      function cloudOne(position,position_y, width_cloud, color){
        if(color === 'dark'){
          fill(120,120,120)
        }
        if(color === 'light'){
          fill(210, 210, 210)
        }
        
        ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
        ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
      }
      cloudOne(x_position_clouds,160, 180,'dark')
      cloudOne(x_position_clouds2 ,140, 140,'light')
      cloudOne(x_position_clouds3 ,90, 120, 'dark')
      cloudOne(x_position_clouds4 ,60, 120, 'light')
      cloudOne(x_position_clouds5 ,100, 100, 'dark')
      cloudOne(x_position_clouds6 ,160, 150, 'light')
      cloudOne(x_position_clouds7 ,80, 110, 'dark')
      
    }
  }
  if(weather_conditions === '03n'){

    function draw(){
      background(0,0,0);
      counter_stars += 1
      
      if(counter_stars % 50 === 0){
        switcher_stars = 0
      }
      if(counter_stars % 100 === 0){
        switcher_stars = 1
      }
      if(counter_stars % 150 === 0){
        switcher_stars = 2
      }
  
      if(switcher_stars === 1){
        for(i = 21; i< 25; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 3, 7, 5); 
          pop();
        }
      }
      if(switcher_stars === 2){
        for(i = 26; i< 30; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 3, 7, 5); 
          pop();
        }
      }
     
  
      
      for(i = 0; i<20; i++){
            push();
            translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
            star(0, 0, 2, 5, 5); 
            pop();
          }
        
       
      fill(255,204,0)
      ellipse(width/2,height/2, diameter, diameter)
      fill(0)
      ellipse(width/2+40,height/2, diameter-30, diameter-15)
  
      x_position_clouds -= 1.2
      if(x_position_clouds <= -100){
        x_position_clouds = 650
      }
      x_position_clouds2 -= 1
      if(x_position_clouds2 === -100){
        x_position_clouds2 = 650
      }
      x_position_clouds3 -= 2
      if(x_position_clouds3 === -100){
        x_position_clouds3 = 650
      }
      x_position_clouds4 -= 1.5
      if(x_position_clouds3 <= -100){
        x_position_clouds3 = 650
      }
      
  
      function cloudOne(position,position_y, width_cloud){
        fill(210, 210, 210)
        ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
        ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
      }
      cloudOne(x_position_clouds,160, 180)
      cloudOne(x_position_clouds2 ,140, 140)
      cloudOne(x_position_clouds3 ,90, 120)
      cloudOne(x_position_clouds4 ,60, 120)
      
    }
   
    
    function star(x, y, radius1, radius2, npoints) {
      var angle = TWO_PI / npoints;
      var halfAngle = angle/2.0;
      fill(255,255,255)
      beginShape();
      for (var a = - 0.5 * PI / npoints; a < TWO_PI; a += angle) {
        var sx = x + cos(a) * radius2;
        var sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a+halfAngle) * radius1;
        sy = y + sin(a+halfAngle) * radius1;
        vertex(sx, sy);
      }
      endShape(CLOSE);
    }
  }

  if(weather_conditions === '04n'){

    function draw(){
      background(0,0,0);
      counter_stars += 1
      
      if(counter_stars % 50 === 0){
        switcher_stars = 0
      }
      if(counter_stars % 100 === 0){
        switcher_stars = 1
      }
      if(counter_stars % 150 === 0){
        switcher_stars = 2
      }
  
      if(switcher_stars === 1){
        for(i = 21; i< 25; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 3, 7, 5); 
          pop();
        }
      }
      if(switcher_stars === 2){
        for(i = 26; i< 30; i++){
          push();
          translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
          star(0, 0, 3, 7, 5); 
          pop();
        }
      }
     
  
      
      for(i = 0; i<20; i++){
            push();
            translate(width * star_pos_list[i][0], height * star_pos_list[i][1]);
            star(0, 0, 2, 5, 5); 
            pop();
          }
        
       
      fill(255,204,0)
      ellipse(width/2,height/2, diameter, diameter)
      fill(0)
      ellipse(width/2+40,height/2, diameter-30, diameter-15)
  
      x_position_clouds -= 1.2
      if(x_position_clouds <= -100){
        x_position_clouds = 650
      }
      x_position_clouds2 -= 0.7
      if(x_position_clouds2 === -100){
        x_position_clouds2 = 650
      }
      x_position_clouds3 -= 2
      if(x_position_clouds3 === -100){
        x_position_clouds3 = 650
      }
      x_position_clouds4 -= 1.5
      if(x_position_clouds4 <= -100){
        x_position_clouds4 = 650
      }
      x_position_clouds5 -= 0.9
      if(x_position_clouds5 <= -100){
        x_position_clouds5 = 650
      }
      x_position_clouds6 -= 1.4
      if(x_position_clouds6 <= -100){
        x_position_clouds6 = 650
      }
      x_position_clouds7 -= 1.8
      if(x_position_clouds7 <= -100){
        x_position_clouds7 = 650
      }
      
  
      function cloudOne(position,position_y, width_cloud, color){
        if(color === 'dark'){
          fill(120,120,120)
        }
        if(color === 'light'){
          fill(210, 210, 210)
        }
        
        ellipse(position,position_y, width_cloud,width_cloud * 0.16667)
        ellipse(position+width_cloud * 0.38889,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position-width_cloud * 0.4,position_y - width_cloud * 0.15,width_cloud * 0.38889 ,width_cloud * 0.38889)
        ellipse(position,position_y - width_cloud * (2/9),width_cloud * 0.61111 ,width_cloud * 0.5556)
      }
      cloudOne(x_position_clouds,160, 180,'dark')
      cloudOne(x_position_clouds2 ,140, 140,'light')
      cloudOne(x_position_clouds3 ,90, 120, 'dark')
      cloudOne(x_position_clouds4 ,60, 120, 'light')
      cloudOne(x_position_clouds5 ,100, 100, 'dark')
      cloudOne(x_position_clouds6 ,160, 150, 'light')
      cloudOne(x_position_clouds7 ,80, 110, 'dark')
      
    }
   
    
    function star(x, y, radius1, radius2, npoints) {
      var angle = TWO_PI / npoints;
      var halfAngle = angle/2.0;
      fill(255,255,255)
      beginShape();
      for (var a = - 0.5 * PI / npoints; a < TWO_PI; a += angle) {
        var sx = x + cos(a) * radius2;
        var sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a+halfAngle) * radius1;
        sy = y + sin(a+halfAngle) * radius1;
        vertex(sx, sy);
      }
      endShape(CLOSE);
    }
  }